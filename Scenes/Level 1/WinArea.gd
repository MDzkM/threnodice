extends Area

func _on_WinArea_body_entered(_body):
	global.current_level = 2
	global.completed_levels = 1
	return get_tree().change_scene("res://Scenes/Level Complete/Level Complete.tscn")
