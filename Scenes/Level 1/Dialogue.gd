extends KinematicBody

var moved
var removed

func _ready():
	moved = false
	removed = false
	
func _physics_process(_delta):
	if !moved and global.can_move:
		moved = true
		translation.y += 9.7
	if !removed and global.bottom_side != 6:
		removed = true
		translation.y -= 9.7
