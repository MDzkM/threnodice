extends Area

var splash

func _ready():
	splash = get_parent().get_node("Splash")

func _on_GameOverArea2_body_exited(_body):
	global.can_move = false
	splash.play()
