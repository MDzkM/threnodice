extends Label

func _ready():
	if global.playing:
		self.visible = true
	else:
		self.visible = false
