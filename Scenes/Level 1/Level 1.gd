extends Spatial

func _ready():
	global.playing = true
	global.can_move = false
	
func _process(_delta):
	get_input()

func get_input():
	if Input.is_action_just_pressed("pause"):
		global.playing = false
		return get_tree().change_scene(str("res://Scenes/Menu Screen/Menu Screen.tscn"))
	if Input.is_action_just_pressed("restart"):
		return get_tree().reload_current_scene()
