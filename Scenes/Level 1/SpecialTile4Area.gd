extends Area

var activated

func _ready():
	activated = false

func _on_SpecialTile4Area_body_entered(_body):
	if !activated and global.bottom_side == 6:
		activated = true
