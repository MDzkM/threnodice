extends GridMap

var moved

func _ready():
	moved = false

func _physics_process(_delta):
	if get_parent().get_node("SpecialTile2Area").activated and !moved:
		collision_layer = 1
		collision_mask = 1
		translation.y += 2
		moved = true
