extends KinematicBody

var gravity = Vector3.DOWN * 50  # strength of gravity
var speed = 240  # movement speed
var velocity = Vector3.ZERO
var sides

func _ready():
	if global.playing:
		self.visible = true
		sides = [6, 1, 5, 2, 3, 4]
	else:
		self.visible = false

func get_input():
	velocity.x = 0
	velocity.z = 0
	if global.can_move:
		var temp
		if Input.is_action_just_pressed("right"):
			self.rotate_x(-PI/2)
			velocity.z -= speed
			temp = sides[0]
			sides[0] = sides[2]
			sides[2] = sides[1]
			sides[1] = sides[3]
			sides[3] = temp
			global.bottom_side = sides[0]
		if Input.is_action_just_pressed("left"):
			self.rotate_x(PI/2)
			velocity.z += speed
			temp = sides[0]
			sides[0] = sides[3]
			sides[3] = sides[1]
			sides[1] = sides[2]
			sides[2] = temp
			global.bottom_side = sides[0]
		if Input.is_action_just_pressed("up"):
			self.rotate_z(PI/2)
			velocity.x -= speed
			temp = sides[0]
			sides[0] = sides[5]
			sides[5] = sides[1]
			sides[1] = sides[4]
			sides[4] = temp
			global.bottom_side = sides[0]
		if Input.is_action_just_pressed("down"):
			self.rotate_z(-PI/2)
			velocity.x += speed
			temp = sides[0]
			sides[0] = sides[4]
			sides[4] = sides[1]
			sides[1] = sides[5]
			sides[5] = temp
			global.bottom_side = sides[0]
	
	return self.move_and_slide(velocity)

func _physics_process(delta):
	velocity += gravity * delta
	get_input()
