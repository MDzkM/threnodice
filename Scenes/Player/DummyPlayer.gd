extends KinematicBody

func _ready():
	if global.playing:
		self.visible = true
	else:
		self.visible = false

func get_input():
	if global.can_move:
		if Input.is_action_just_pressed("left"):
			self.rotate_x(-PI/2)
		if Input.is_action_just_pressed("right"):
			self.rotate_x(PI/2)
		if Input.is_action_just_pressed("down"):
			self.rotate_z(PI/2)
		if Input.is_action_just_pressed("up"):
			self.rotate_z(-PI/2)

func _physics_process(_delta):
	get_input()
