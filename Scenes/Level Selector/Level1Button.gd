extends LinkButton

func _ready():
	if global.completed_levels >= 1:
		modulate = Color(1, 1, 0)

func _on_Level1Button_pressed():
	global.playing = true
	return get_tree().change_scene(str("res://Scenes/Level 1/Level 1.tscn"))
