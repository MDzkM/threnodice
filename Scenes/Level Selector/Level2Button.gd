extends LinkButton

func _ready():
	if global.completed_levels >= 2:
		modulate = Color(1, 1, 0)

func _on_Level2Button_pressed():
	if global.completed_levels >= 1:
		global.playing = false
		return get_tree().change_scene("res://Scenes/The End/The End.tscn")
