extends LinkButton

func _on_ResumeButton_pressed():
	global.playing = true
	return get_tree().change_scene(str("res://Scenes/Level " + str(global.current_level) + "/Level " + str(global.current_level) + ".tscn"))
