tool
extends Spatial

# Simple Tool script for quickly generating trimeshes for Gridmap Tiles.
# Attach to root node of Tile Scene
# Smack Generate Trimeshes in properties window

export (bool) var generate_trimeshes = false setget generate_trimesh_collisions

func generate_trimesh_collisions(generate_trimeshes):
	var meshes = get_children()
	for mesh in meshes:
		if mesh.has_method("create_trimesh_collision"):
			if mesh.get_child_count() == 0:
				mesh.create_trimesh_collision()
				print(mesh.name + ": Trimesh created.")
			else:
				print(mesh.name + ": Trimesh exists. Skipping...")
		else:
			print(mesh.name + ": Not Mesh Instance. Skipping...")
	generate_trimeshes = false
